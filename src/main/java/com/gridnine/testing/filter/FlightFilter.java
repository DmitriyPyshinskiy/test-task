package com.gridnine.testing.filter;

import com.gridnine.testing.model.Flight;
import com.gridnine.testing.rule.Rule;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class FlightFilter {
    private static final FlightFilterBuilder flightFilterBuilder = new FlightFilterBuilder();

    private final List<Rule> rules = new ArrayList<>();

    private List<Flight> flights = new ArrayList<>();

    public List<Flight> filterAndGet() {
        rules.forEach(rule -> {
            flights = flights.stream().filter(rule.getPredicate()).collect(Collectors.toList());
            System.out.println(
                    String.format(
                            "\nFlights were filtered. The Rule with description: \"%s\" was applied.",
                            rule.getDescription()
                    ));
            flights.forEach(System.out::println);
        });
        return flights;
    }

    public static FlightFilterBuilder builder() {
        return flightFilterBuilder;
    }


    public static class FlightFilterBuilder {
        private final FlightFilter flightFilter = new FlightFilter();

        public FlightFilterBuilder withFlights(List<Flight> flights) {
            flightFilter.flights.addAll(flights);
            return this;
        }

        public FlightFilterBuilder withRules(List<Rule> rules) {
            flightFilter.rules.addAll(rules);
            return this;
        }

        public FlightFilterBuilder withRule(Rule rule) {
            flightFilter.rules.add(rule);
            return this;
        }

        public FlightFilter build() {
            return flightFilter;
        }
    }
}
