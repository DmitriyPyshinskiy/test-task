package com.gridnine.testing.rule.impl;

import com.gridnine.testing.model.Flight;
import com.gridnine.testing.model.Segment;
import com.gridnine.testing.rule.Rule;

import java.time.Duration;
import java.util.function.Predicate;

public class TooLongStayOnTheGroundRule implements Rule {
    private static final TooLongStayOnTheGroundRule instance = new TooLongStayOnTheGroundRule();

    private TooLongStayOnTheGroundRule() {}

    public static TooLongStayOnTheGroundRule getInstance() {
        return instance;
    }

    @Override
    public Predicate<Flight> getPredicate() {
        return flight -> {
            if(flight.getSegments().size() == 1) {
                return true;
            }
            for (int i = 0; i < flight.getSegments().size()-1; i++) {
                Segment firstSegment = flight.getSegments().get(i);
                Segment secondSegment = flight.getSegments().get(i+1);
                if(Duration.between(firstSegment.getArrivalDate(), secondSegment.getDepartureDate()).toHours() < 2) {
                    return true;
                }
            }
            return false;
        };
    }

    @Override
    public String getDescription() {
        return "filter flights witch stay on the ground more than 2 hours";
    }
}
