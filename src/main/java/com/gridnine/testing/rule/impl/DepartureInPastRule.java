package com.gridnine.testing.rule.impl;

import com.gridnine.testing.model.Flight;
import com.gridnine.testing.rule.Rule;

import java.time.LocalDateTime;
import java.util.function.Predicate;

public class DepartureInPastRule implements Rule {
    private static final Rule instance = new DepartureInPastRule();

    private DepartureInPastRule() {}

    public static Rule getInstance() {
        return instance;
    }

    @Override
    public Predicate<Flight> getPredicate() {
        return flight -> flight.getSegments().stream()
                .anyMatch(segment -> segment.getDepartureDate().isAfter(LocalDateTime.now()));
    }

    @Override
    public String getDescription() {
        return "filter flights with departure time in the Past";
    }
}
