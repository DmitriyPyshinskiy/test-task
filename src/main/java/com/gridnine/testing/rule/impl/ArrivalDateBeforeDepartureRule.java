package com.gridnine.testing.rule.impl;

import com.gridnine.testing.model.Flight;
import com.gridnine.testing.rule.Rule;

import java.util.function.Predicate;

public class ArrivalDateBeforeDepartureRule implements Rule {

    private static final Rule instance = new ArrivalDateBeforeDepartureRule();

    private ArrivalDateBeforeDepartureRule() {}

    public static Rule getInstance() {
        return instance;
    }


    @Override
    public Predicate<Flight> getPredicate() {
        return flight -> flight.getSegments().stream()
                .anyMatch(segment -> segment.getArrivalDate().isAfter(segment.getDepartureDate()));
    }

    @Override
    public String getDescription() {
        return "filter flights with arrival date < departure date";
    }
}
