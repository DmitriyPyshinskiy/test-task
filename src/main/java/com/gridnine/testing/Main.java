package com.gridnine.testing;

import com.gridnine.testing.filter.FlightFilter;
import com.gridnine.testing.model.Flight;
import com.gridnine.testing.rule.impl.ArrivalDateBeforeDepartureRule;
import com.gridnine.testing.rule.impl.DepartureInPastRule;
import com.gridnine.testing.rule.impl.TooLongStayOnTheGroundRule;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Flight> flights = FlightBuilder.createFlights();
        System.out.println("Original flights: ");
        flights.forEach(System.out::println);

        FlightFilter flightFilter = FlightFilter.builder()
                .withFlights(flights)
                .withRule(DepartureInPastRule.getInstance())
                .withRule(ArrivalDateBeforeDepartureRule.getInstance())
                .withRule(TooLongStayOnTheGroundRule.getInstance())
                .build();

        flightFilter.filterAndGet();

    }
}