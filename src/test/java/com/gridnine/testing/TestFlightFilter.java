package com.gridnine.testing;

import com.gridnine.testing.filter.FlightFilter;
import com.gridnine.testing.model.Flight;
import com.gridnine.testing.rule.Rule;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;
import java.util.function.Predicate;

public class TestFlightFilter {
    private final List<Flight> testFlights = FlightBuilder.createFlights();

    @Test
    public void testFilterAllFlights() {
        Rule testRule = new Rule() {
            @Override
            public Predicate<Flight> getPredicate() {
                return flight -> false;
            }

            @Override
            public String getDescription() {
                return "test rule";
            }
        };
        FlightFilter flightFilter = FlightFilter.builder()
                .withFlights(testFlights)
                .withRule(testRule)
                .build();
        List<Flight> filteredFlights = flightFilter.filterAndGet();
        Assert.assertTrue(filteredFlights.isEmpty());
    }
}
